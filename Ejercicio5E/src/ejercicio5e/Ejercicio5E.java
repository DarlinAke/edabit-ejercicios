
package ejercicio5e;
import java.util.stream.IntStream;

public class Ejercicio5E {

  public static boolean changeEnough(int[] change, double amountDue) {
       
        //DARLIN EDEY CAHUICH AKE
        //Convenience Store

	
		//quarter: 25 cents / $0.25
		//dime: 10 cents / $0.10
		//nickel: 5 cents / $0.05
		//penny: 1 cent / $0.01
		
		
		double[] value = new double[] {0.25, 0.10, 0.05, 0.01};
		
		boolean result = IntStream.range(0, change.length).mapToDouble(i -> change[i] * value[i]).sum() >= amountDue;
		System.out.println(result);
		
		return result;
	}	
}

    
    